package com.university.ip.ui.editor.filters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import com.university.ip.R

class Sharp:Filters() {
    override fun Show() {
        TODO("Not yet implemented")
    }

    override fun Hide() {
        TODO("Not yet implemented")
    }

    override fun GetViewHolder(context: Context, parent: ViewGroup): ViewHolder {
        var view= LayoutInflater.from(context).inflate(R.layout.hight_pass_filters,parent,false)

        return ViewHolder(view)
    }
    var alfa:Int=60
    var beta:Int=0
    var gama:Int=0
    override fun Bind(holder: ViewHolder) {
        var b:Button
        b=holder.itemView.findViewById(R.id.button)

        var al:SeekBar =holder.itemView.findViewById(R.id.seekBar_alfa)
        al.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alfa=progress;
                editor?.sharp(bitmap,alfa,beta, gama)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
         al =holder.itemView.findViewById(R.id.seekBar_beta)
        al.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                beta=progress;
                editor?.sharp(bitmap,alfa,beta, gama)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
        al =holder.itemView.findViewById(R.id.seekBar_gama)
        al.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                gama=progress;
                editor?.sharp(bitmap,alfa,beta, gama)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
        b.setOnClickListener(View.OnClickListener {

            editor?.sharp(bitmap,alfa,beta, gama)
        })
    }
}